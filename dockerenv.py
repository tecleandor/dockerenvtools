import os

from flask import Flask
from flask import jsonify
from flask import request
from logging.config import dictConfig

dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s"
            }
        },
        "handlers": {
            "wsgi": {
                "class": "logging.StreamHandler",
                "stream": "ext://flask.logging.wsgi_errors_stream",
                "formatter": "default",
            }
        },
        "root": {"level": "INFO", "handlers": ["wsgi"]},
    }
)

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "Hello, World!"


@app.route("/env/")
def list_environment():
    environment = {}
    for param in os.environ.keys():
        environment[param] = "censored"  # os.environ[param]
        app.logger.info("%s=%s", param, os.environ[param])
    return jsonify(environment)


@app.route("/env/<string:envkey>")
def get_environment(envkey):
    envval = {}
    try:
        envval[envkey] = "censored"  # os.environ[envkey]
        app.logger.info("%s=%s", envkey, os.environ[envkey])
        return jsonify(envval)
    except KeyError:
        return "This environment variable doesn't exist", 404


@app.route("/header/")
def list_headers():
    headers = {}
    for param in request.headers.keys():
        headers[param] = request.headers[param]
        app.logger.info("%s=%s", param, request.headers[param])
    return jsonify(headers)


@app.route("/header/<string:headerkey>")
def get_header(headerkey):
    envval = {}
    try:
        envval[headerkey] = request.headers[headerkey]
        app.logger.info("%s=%s", headerkey, request.headers[headerkey])
        return jsonify(envval)
    except KeyError:
        return "This header doesn't exist", 404
